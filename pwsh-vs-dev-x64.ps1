# Based on: https://gist.github.com/ArthurHNL/b29f008cbc7a159a6ae82a7152b1cb2a

# Determine Visual Studio path (see https://github.com/microsoft/vswhere/wiki/Find-VC)
$vsPath = &"${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -latest -products * -requires Microsoft.VisualStudio.Component.VC.Tools.x86.x64 -property installationpath

Write-Host "Visual Studio path: $vsPath"

Import-Module (Get-ChildItem $vsPath -Recurse -File -Filter Microsoft.VisualStudio.DevShell.dll).FullName
Enter-VsDevShell -VsInstallPath $vsPath -SkipAutomaticLocation -DevCmdArguments '-arch=x64 -host_arch=x64'
