#cl.exe /EHsc test.cpp

Write-Output "Python: ${env:python_exe}"
Write-Output "Meson:  ${env:meson_py}"

if (${env:python_exe} -eq $null) {
	Write-Error "ERROR: No python.exe path given"
	exit 1
}
if (${env:meson_py} -eq $null) {
	Write-Error "ERROR: No meson.py path given"
	exit 1
}

& "${env:python_exe}" "${env:meson_py}" builddir
ninja -C builddir

Get-ChildItem
Get-ChildItem builddir
