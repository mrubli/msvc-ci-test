#!/bin/bash

set -euo pipefail

#g++ -o test test.cpp

cat /etc/os-release

apt-get update
apt-get install -y meson

meson builddir
ninja -C builddir
